import java.awt.*;
import javax.swing.*;

public class LabelPro2 extends Label{
	public LabelPro2(String n1, String n2, String n3){
		l1 = new JLabel(n1);
		l2 = new JLabel(n2);
		l3 = new JLabel(n3);

		this.setLayout(new FlowLayout());
		this.setBackground(Color.CYAN);

		this.add(l1);
		this.add(l2);
		this.add(l3);
	}//Constructor
}//LabelPro2