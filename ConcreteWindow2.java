import javax.swing.*;
import java.awt.*;

public class ConcreteWindow2 extends AbstractWindow{

	public ConcreteWindow2(){
		super("Concrete Window 2");

		panButton = new ButtonPro2("Botão 1 Pro 2", "Botão 2 Pro 2", "Botão 3 Pro 2");
		panLabel = new LabelPro2("Rótulo 1 Pro 2", "Rótulo 2 Pro 2", "Rótulo 3 Pro 2");

		Container c = getContentPane();
		c.setLayout(new BorderLayout());

		c.add(panButton, BorderLayout.WEST);
		c.add(panLabel, BorderLayout.EAST);

		setSize(300,300);
		setLocation(300,100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}// Constructor

}// ConcreteWindow2