/*
	Using - Abstract Factory

	Abstract Factory: AbstractWindow
	Concrete Factory: ConcreteWindow1, ConctreteWindow2
	Abstract Products: Button, Label
	Concrete Products: ButtonPro1, ButtonPro2, LabelPro1, LabelPro2
*/

import javax.swing.*;
import java.awt.*;

public abstract class AbstractWindow extends JFrame{
	protected Button panButton;
	protected Label panLabel;

	public AbstractWindow(){
		this("Editor");
	}//Constructor

	public AbstractWindow(String n){
		super(n);
	}//Constructor
}// AbstractWindow