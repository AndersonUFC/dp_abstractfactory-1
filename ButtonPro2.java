import java.awt.*;
import javax.swing.*;

public class ButtonPro2 extends Button{
	public ButtonPro2(String n1, String n2, String n3){
		b1 = new JButton(n1);
		b2 = new JButton(n2);
		b3 = new JButton(n3);

		this.setLayout(new BorderLayout());
		this.setBackground(Color.GRAY);

		this.add(b1, BorderLayout.NORTH);
		this.add(b2, BorderLayout.CENTER);
		this.add(b3, BorderLayout.SOUTH);
	}//Constructor
}//ButtonPro2