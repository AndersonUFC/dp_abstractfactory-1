import javax.swing.*;
import java.awt.*;

public class ConcreteWindow1 extends AbstractWindow{

	public ConcreteWindow1(){
		super("Concrete Window 1");

		panButton = new ButtonPro1("Botão 1 Pro 1", "Botão 2 Pro 1", "Botão 3 Pro 1");
		panLabel = new LabelPro1("Rótulo 1 Pro 1", "Rótulo 2 Pro 1", "Rótulo 3 Pro 1");

		Container c = getContentPane();
		c.setLayout(new BorderLayout());

		c.add(panButton, BorderLayout.WEST);
		c.add(panLabel, BorderLayout.EAST);

		setSize(500,500);
		setLocation(500,100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}// Constructor

}// ConcreteWindow1