import java.awt.*;
import javax.swing.*;

public class LabelPro1 extends Label{
	public LabelPro1(String n1, String n2, String n3){
		l1 = new JLabel(n1);
		l2 = new JLabel(n2);
		l3 = new JLabel(n3);

		this.setLayout(new BorderLayout());
		this.setBackground(Color.RED);

		this.add(l1, BorderLayout.NORTH);
		this.add(l2, BorderLayout.CENTER);
		this.add(l3, BorderLayout.SOUTH);
	}//Constructor
}//LabelPro1