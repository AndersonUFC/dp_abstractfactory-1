import java.awt.*;
import javax.swing.*;

public class ButtonPro1 extends Button{
	public ButtonPro1(String n1, String n2, String n3){
		b1 = new JButton(n1);
		b2 = new JButton(n2);
		b3 = new JButton(n3);

		this.setLayout(new FlowLayout());
		this.setBackground(Color.LIGHT_GRAY);

		this.add(b1);
		this.add(b2);
		this.add(b3);
	}//Constructor
}//ButtonPro1